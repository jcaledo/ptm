# ptm

R package for the analysis of post-translational modifications, with particular emphasis on the sulfoxidation (oxidation) of methionine residues.

Extensive and detailed documentation related to this package can be found at https://metositeptm.com

## Citing ptm

Aledo, JC. ptm: an R package for the study of methionine sulfoxidation and other posttranslational modifications.
Bioinformatics, Volume 37, Issue 21, 1 November 2021, Pages 3979–3980, https://doi.org/10.1093/bioinformatics/btab348

## Contributing

We are always interested in improving and adding additional functionality to _ptm_. If you have ideas, suggestions or code that your would like to distribute as part of this package, please contact us (see below).
You are encouraged to contribute your code or issues directly to this repository. For details on how to do this, please see the [developer wiki]().

## Contact

Your are welcome to:

* Submit suggestions and bug-reports at: https://bitbucket.org/jcaledo/ptm/issues
* Send a pull request on: https://bitbucket.org/jcaledo/ptm/pull-request
* Send us an email to: caledo@uma.es
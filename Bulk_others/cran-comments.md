## Resubmission

This is a resubmission. Now, packages in Suggests (muscle, Biostrings, and KEGGREST) have been used conditionally. 

## Test environments

* local OS X install, R 4.3.2

* win-builder (devel and release)

## R CMD check results

There were no ERRORs, WARNINGs or NOTEs. 


## Downstream dependencies

There are currently no downstream dependencies for this package.

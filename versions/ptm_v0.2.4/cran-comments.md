## Resubmission

This is a resubmission. Now a proper reference describing the method in the package has been included in the DESCRIPTION. In addition, now we have been careful to reset the user par() after running the vignettes code. 


## Test environments

* local OS X install, R 4.0.2

* local Linux install, R 4.0.4

* win-builder (devel and release)

## R CMD check results

There were no ERRORs, WARNINGs or NOTEs. 


## Downstream dependencies

There are currently no downstream dependencies for this package.

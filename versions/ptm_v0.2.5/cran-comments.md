## Resubmission

This is a resubmission. Now a proper Authors@R field has been added to the DESCRIPTION as requested. 


## Test environments

* local OS X install, R 4.1.2

* win-builder (devel and release)

## R CMD check results

There were no ERRORs, WARNINGs or NOTEs. 


## Downstream dependencies

There are currently no downstream dependencies for this package.

## Submission

This is a new submission because the previous version of the package was archived on CRAN, as some issues were not corrected in time. Now, the issues has been addressed and packages in Suggests (muscle, Biostrings, and KEGGREST) have been used conditionally. In addition, the structure of the data reported by the function meto.scan() has been simplified.

## Test environments

* local OS X install, R 4.3.2

* win-builder (devel and release)

## R CMD check results

There were no ERRORs, WARNINGs or NOTEs. 


## Downstream dependencies

There are currently no downstream dependencies for this package.

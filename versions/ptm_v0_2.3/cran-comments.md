## Resubmission

This is a  resubmission. Now, in the current version:

* Those functions that use Internet resources have been modified to fail gracefully with an informative message if the resource is not available, instead of giving a check warning or error.

## Test environments

* local OS X install, R 4.0.2

* local Linux install, R 4.0.4

* win-builder (devel and release)

## R CMD check results

There were no ERRORs, WARNINGs or NOTEs. 


## Downstream dependencies

There are currently no downstream dependencies for this package.
